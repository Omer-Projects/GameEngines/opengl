#pragma once

class VertexBuffer
{
private:
	unsigned int m_RenderID;
	unsigned int m_Size;

public:
	VertexBuffer();
	VertexBuffer(const void* data, unsigned int size);
	~VertexBuffer();

	void Load(const void* data, unsigned int size);

	void Bind() const;
	void Unbind() const;

	void ChangeData(const void* data);
};