#pragma once

#include "imgui/imgui.h"

#include "Tests/Test.h"

#include "Tests/TestClearColor.h"
#include "Tests/TestMultiModels.h"

using namespace test;

class TestsMenu : public Test
{
private:
	Test*& m_ChosenTest;

public:
	TestsMenu(Test*& test)
		: m_ChosenTest(test)
	{

	}

	void OnRender() override
	{
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT);
	}

	void OnImGuiRender() override
	{
		if (ImGui::Button("Clear Color"))
		{
			m_ChosenTest = new TestClearColor();
			return;
		}

		if (ImGui::Button("Multi Models"))
		{
			m_ChosenTest = new TestMultiModels();
			return;
		}
	}
};
