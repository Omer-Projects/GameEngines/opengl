#pragma once

#include <vector>

#include "Renderer.h"

struct VertexLayoutElement
{
	unsigned int Count;
	unsigned int Type;
	unsigned char Normalized;

	static unsigned int GetSizeOfType(unsigned int type)
	{
		switch (type)
		{
			case GL_FLOAT:
				return sizeof(GLfloat);

			case GL_UNSIGNED_INT:
				return sizeof(GLuint);

			case GL_UNSIGNED_BYTE:
				return sizeof(GLbyte);
		}

		ASSERT(false);
		return 0;
	}
};

class VertexLayoutBuffer
{
private:
	std::vector<VertexLayoutElement> m_Elements;

	unsigned int m_Stride;

public:
	VertexLayoutBuffer()
		: m_Stride(0) {}

	template<typename T>
	void Push(unsigned int count)
	{
		static_assert(false);
	}

	template<>
	void Push<float>(unsigned int count)
	{
		m_Elements.push_back({count, GL_FLOAT, GL_FALSE});
		m_Stride += count * VertexLayoutElement::GetSizeOfType(GL_FLOAT);
	}

	template<>
	void Push<unsigned int>(unsigned int count)
	{
		m_Elements.push_back({ count, GL_UNSIGNED_INT, GL_FALSE });
		m_Stride += count * VertexLayoutElement::GetSizeOfType(GL_UNSIGNED_INT);
	}

	template<>
	void Push<unsigned char>(unsigned int count)
	{
		m_Elements.push_back({ count, GL_UNSIGNED_BYTE, GL_FALSE });
		m_Stride += count * VertexLayoutElement::GetSizeOfType(GL_UNSIGNED_BYTE);
	}

	inline std::vector<VertexLayoutElement> GetElements() const { return m_Elements; }
	inline unsigned int GetStride() const { return m_Stride; }
};

