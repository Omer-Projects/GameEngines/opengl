#include "VertexBuffer.h"

#include "Renderer.h"

VertexBuffer::VertexBuffer()
{

}

VertexBuffer::VertexBuffer(const void* data, unsigned int size)
{
    Load(data, size);
}

VertexBuffer::~VertexBuffer()
{
    glDeleteBuffers(1, &m_RenderID);
}

void VertexBuffer::Load(const void* data, unsigned int size)
{
    m_Size = size;

    glGenBuffers(1, &m_RenderID);
    glBindBuffer(GL_ARRAY_BUFFER, m_RenderID);
    glBufferData(GL_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);
}

void VertexBuffer::Bind() const
{
    glBindBuffer(GL_ARRAY_BUFFER, m_RenderID);
}

void VertexBuffer::Unbind() const
{
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void VertexBuffer::ChangeData(const void* data)
{
    glBindBuffer(GL_ARRAY_BUFFER, m_RenderID);
    glBufferData(GL_ARRAY_BUFFER, m_Size, data, GL_STATIC_DRAW);
}