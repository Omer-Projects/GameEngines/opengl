#include "IndexBuffer.h"

#include <GL/glew.h>

IndexBuffer::IndexBuffer()
{
}

IndexBuffer::IndexBuffer(const unsigned int* data, unsigned int count)
{
    Load(data, count);
}

IndexBuffer::~IndexBuffer()
{
    glDeleteBuffers(1, &m_RenderID);
}

void IndexBuffer::Load(const unsigned int* data, unsigned int count)
{
    m_Count = count;
    glGenBuffers(1, &m_RenderID);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_RenderID);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(unsigned int), data, GL_STATIC_DRAW);
}

void IndexBuffer::Bind() const
{
    glBindBuffer(GL_ARRAY_BUFFER, m_RenderID);
}

void IndexBuffer::Unbind() const
{
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}
