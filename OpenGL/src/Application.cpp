#include "Core.h"

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "imgui/imgui.h"
#include "imgui/imgui_impl_glfw_gl3.h"

#include "Renderer.h"

#include "VertexBuffer.h"
#include "VertexLayoutBuffer.h"
#include "IndexBuffer.h"
#include "VertexArray.h"
#include "Shader.h"
#include "Texture.h"

#include "TestsMenu.h"

void theGame(GLFWwindow* window, bool& imGuiActive)
{
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    Renderer renderer;

    ImGui::CreateContext();
    ImGui_ImplGlfwGL3_Init(window, true);
    ImGui::StyleColorsDark();

    test::Test* test = nullptr;
    TestsMenu testsMenu(test);

    while (!glfwWindowShouldClose(window))
    {
        renderer.Clear();

        if (test != nullptr)
        {
            test->OnUpdate(0.0f);
            test->OnRender();
        }
        else
        {
            testsMenu.OnUpdate(0.0f);
            testsMenu.OnRender();
        }

        // ImGui Render
        if (imGuiActive)
        {
            ImGui_ImplGlfwGL3_NewFrame();

            if (test != nullptr)
            {
                bool endTest = ImGui::Button("Tests Menu");
                test->OnImGuiRender();

                if (endTest)
                {
                    test = nullptr;
                }
            }
            else
            {
                testsMenu.OnImGuiRender();
            }
            ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);

            ImGui::Render();
            ImGui_ImplGlfwGL3_RenderDrawData(ImGui::GetDrawData());
        }

        // Swap front and back buffers
        glfwSwapBuffers(window);

        // Poll for and process events
        glfwPollEvents();

        if (imGuiActive == true && test != nullptr && !(test->ShowImGui()))
        {
            ImGui::DestroyContext();
            imGuiActive = false;
        }
    }
}

int main()
{
    GLFWwindow* window;

    // Initialize GLFW
    if (!glfwInit())
    {
        return -1;
    }

    // Create Window
    window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "OpenGL", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        return -1;
    }

    glfwMakeContextCurrent(window);
    glfwSwapInterval(1);

    // Initialize GLEW
    if (glewInit() != GLEW_OK)
    {
        glfwTerminate();
        return -1;
    }

    std::cout << "OpenGL Version: " << glGetString(GL_VERSION) << std::endl;

    // The Main Application
    bool imGuiActive = true;
    theGame(window, imGuiActive);

    // End
    if (imGuiActive)
    {
        ImGui_ImplGlfwGL3_Shutdown();
        ImGui::DestroyContext();
    }

    glfwTerminate();
    
    return 0;
}