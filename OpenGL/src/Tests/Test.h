#pragma once

#include "Core.h"

#define HIDE_UI() bool ShowImGui() const override { return false; }

namespace test
{
	class Test
	{
	public:
		Test() { }
		virtual ~Test() { }

		virtual bool ShowImGui() const { return true; }

		virtual void OnUpdate(float deltaTime) {}
		virtual void OnRender() {}
		virtual void OnImGuiRender() {}
	};
}