#pragma once

#include "imgui/imgui.h"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

#include "Renderer.h"
#include "VertexArray.h"
#include "VertexBuffer.h"
#include "IndexBuffer.h"
#include "Shader.h"
#include "Texture.h"

#include "Test.h"

namespace test
{
	class TestMultiModels : public Test
	{
	private:
		Renderer renderer;

		glm::mat4 m_Proj;

		glm::vec3 m_TranslateCamera;
		glm::vec3 m_TranslateModel;

		Shader m_Shader;
		VertexArray m_VA;
		VertexBuffer m_VB;
		IndexBuffer m_IB;
		Texture m_Texture;

		int m_ColorMode;
		ImVec4 m_SelectedColor;
		float m_Size;

		float m_R;
		float m_Increment;

	public:
		TestMultiModels();
		~TestMultiModels();

		void OnUpdate(float deltaTime) override;
		void OnRender() override;
		void OnImGuiRender() override;

	public:
		void ResetSettings();
	};
}