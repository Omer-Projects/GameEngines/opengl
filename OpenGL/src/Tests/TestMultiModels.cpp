#include "TestMultiModels.h"

#include "Core.h"

#include "VertexLayoutBuffer.h"

namespace test
{
	TestMultiModels::TestMultiModels()
		: m_Shader("res/shaders/Basic.shader"), m_Texture("res/textures/2-swords.png"),
		  m_R(0.0f), m_Increment(0.05f)
	{
        ResetSettings();

		float positions[] = {
			-1.0f * m_Size / 2, -1.0f * m_Size / 2, 0.0f, 0.0f, // 0
			 1.0f * m_Size / 2, -1.0f * m_Size / 2, 1.0f, 0.0f, // 1
			 1.0f * m_Size / 2,  1.0f * m_Size / 2, 1.0f, 1.0f, // 2
			-1.0f * m_Size / 2,  1.0f * m_Size / 2, 0.0f, 1.0f, // 3
		};

		unsigned int indices[] = {
			0, 1, 2,
			2, 3, 0,
		};

        m_VB.Load(positions, 4 * 4 * sizeof(float));
        VertexLayoutBuffer layout;
        layout.Push<float>(2);
        layout.Push<float>(2);

        m_VA.AddBuffer(m_VB, layout);

        m_IB.Load(indices, 6);

        m_Proj = glm::ortho(0.0f, WINDOW_WIDTH, 0.0f, WINDOW_HEIGHT, -1.0f, 1.0f);

        m_Shader.Bind();

        m_Shader.SetUniform4f("u_Color", 0.0f, 0.0f, 0.0f, 1.0f);
        m_Shader.SetUniform1i("u_ShowTexture", false);

        m_Texture.Bind();
        m_Shader.SetUniform1i("u_Texture", 0);

        m_VA.Unbind();
        m_VB.Unbind();
        m_IB.Unbind();
        m_Shader.Unbind();
	}

	TestMultiModels::~TestMultiModels()
	{

	}

	void TestMultiModels::OnUpdate(float deltaTime)
	{

	}

	void TestMultiModels::OnRender()
	{
        glm::mat4 view = glm::translate(glm::mat4(1.0f), -m_TranslateCamera);
        glm::mat4 model = glm::translate(glm::mat4(1.0f), m_TranslateModel);

        glm::mat4 mvp = m_Proj * view * model;

        float positions[] = {
            -1.0f * m_Size / 2, -1.0f * m_Size / 2, 0.0f, 0.0f, // 0
             1.0f * m_Size / 2, -1.0f * m_Size / 2, 1.0f, 0.0f, // 1
             1.0f * m_Size / 2,  1.0f * m_Size / 2, 1.0f, 1.0f, // 2
            -1.0f * m_Size / 2,  1.0f * m_Size / 2, 0.0f, 1.0f, // 3
        };

        m_VB.ChangeData(positions);

        m_Shader.Bind();
        switch (m_ColorMode)
        {
        case 1:
        {
            m_Shader.SetUniform4f("u_Color", m_R, 0.0f, 0.0f, 1.0f);
        }
        break;
        case 2:
        {
            m_Shader.SetUniform4f("u_Color", m_SelectedColor.x, m_SelectedColor.y, m_SelectedColor.z, m_SelectedColor.w);
        }
        break;
        case 3:
        {
            m_Shader.SetUniform4f("u_Color", m_SelectedColor.x + m_R, m_SelectedColor.y, m_SelectedColor.z, m_SelectedColor.w);
        }
        break;
        }

        m_Shader.SetUniform1i("u_ShowTexture", m_ColorMode == 0);
        m_Shader.SetUniformMat4f("u_MVP", mvp);

        float hefX = (WINDOW_WIDTH / 2) + (WINDOW_WIDTH - m_TranslateModel.x) / 2;
        float hefY = (WINDOW_HEIGHT / 2 - m_TranslateModel.y);

        float arr[] = {
            m_TranslateModel.x, m_TranslateModel.y,
            m_TranslateModel.x, WINDOW_HEIGHT - m_TranslateModel.y,
            WINDOW_WIDTH - m_TranslateModel.x, m_TranslateModel.y,
            WINDOW_WIDTH - m_TranslateModel.x, WINDOW_HEIGHT - m_TranslateModel.y,

            m_TranslateModel.x / 2, m_TranslateModel.y / 2,
            m_TranslateModel.x / 2, WINDOW_HEIGHT - m_TranslateModel.y / 2,
            WINDOW_WIDTH - m_TranslateModel.x / 2, m_TranslateModel.y / 2,
            WINDOW_WIDTH - m_TranslateModel.x / 2, WINDOW_HEIGHT - m_TranslateModel.y / 2,
        };

        for (int i = 0; i < sizeof(arr) / sizeof(float); i += 2)
        {
            model = glm::translate(glm::mat4(1.0f),
                glm::vec3(arr[i], arr[i + 1], m_TranslateModel.z));

            mvp = m_Proj * view * model;

            m_Shader.SetUniformMat4f("u_MVP", mvp);

            renderer.Draw(m_VA, m_IB, m_Shader);
        }

        if (0.0f > m_R || m_R > 1.0f)
        {
            m_Increment *= -1;
        }
        m_R += m_Increment;
	}

	void TestMultiModels::OnImGuiRender()
	{
        ImGui::SliderFloat("Translate Camera X", &m_TranslateCamera.x, -WINDOW_WIDTH, WINDOW_WIDTH);
        ImGui::SliderFloat("Translate Camera Y", &m_TranslateCamera.y, -WINDOW_HEIGHT, WINDOW_HEIGHT);
        ImGui::SliderFloat("Translate Model X", &m_TranslateModel.x, 0, WINDOW_WIDTH);
        ImGui::SliderFloat("Translate Model Y", &m_TranslateModel.y, 0, WINDOW_HEIGHT);
        ImGui::SliderFloat("Size Model", &m_Size, 0, 400.0f);
        ImGui::RadioButton("Texture", &m_ColorMode, 0);
        ImGui::RadioButton("Color Changed", &m_ColorMode, 1);
        ImGui::RadioButton("Selected Color", &m_ColorMode, 2);
        ImGui::RadioButton("Selected Color Changed", &m_ColorMode, 3);


        if (m_ColorMode >= 2)
        {
            ImGui::ColorEdit3("Color", (float*)&m_SelectedColor);
        }

        if (ImGui::Button("Reset"))
        {
            ResetSettings();
        }
	}

    void TestMultiModels::ResetSettings()
    {
        m_TranslateCamera = glm::vec3(0.0f, 0.0f, 0);
        m_TranslateModel = glm::vec3(WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2, 0);
        m_ColorMode = 0;
        m_SelectedColor = ImVec4(0.0f, 0.0f, 1.0f, 1.00f);
        m_Size = 100;
    }
}