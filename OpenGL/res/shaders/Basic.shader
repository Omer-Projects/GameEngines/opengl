#shader vertex
#version 330 core

layout(location = 0) in vec4 position;
layout(location = 1) in vec2 textureCoordinate;

out vec2 v_textureCoordinate;

uniform mat4 u_MVP;

void main()
{
	gl_Position = u_MVP * position;

	v_textureCoordinate = textureCoordinate;
}

#shader fragment
#version 330 core

layout(location = 0) out vec4 color;

in vec2 v_textureCoordinate;

uniform vec4 u_Color;
uniform sampler2D u_Texture;
uniform int u_ShowTexture;

void main()
{
	if (u_ShowTexture == 0)
	{
		color = u_Color;
	}
	else
	{
		vec4 textureColor = texture(u_Texture, v_textureCoordinate);
		color = textureColor;
	}
}